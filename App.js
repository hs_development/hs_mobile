import React from 'react';
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/components/reducers';
import firebase from 'firebase';
import { View, BackHandler } from 'react-native' ;
import { Font } from 'expo';
import Router from './src/Router';
import { NavigationActions } from 'react-navigation'
import { Platform } from 'react-native'


class App extends React.Component {



  componentWillMount() {
      const config = {
          apiKey: "AIzaSyC_Au1e1cJ9vUE3Hh-Qc9Xsr1fY4U0QhX4",
          authDomain: "heraspace-78b4b.firebaseapp.com",
          databaseURL: "https://heraspace-78b4b.firebaseio.com",
          projectId: "heraspace-78b4b",
          storageBucket: "heraspace-78b4b.appspot.com",
          messagingSenderId: "300888233631"
        };
      firebase.initializeApp(config);
  }



  render() {
    console.ignoredYellowBox = ['Remote debugger'];
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <View style={{flex:1}}>
        <Provider store={store}>
          <Router />
        </Provider>
      </View>

    );
  }
}



export default App;
