import React, { Component } from 'react';
import { View, BackHandler, Picker, TextInput, Modal, TouchableHighlight, ToastAndroid } from 'react-native';
import { FontText , Card, CardSection, Header, StatusBar } from './common';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';
import WeightBox from './WeightBox' ;
import DateTimePicker from 'react-native-modal-datetime-picker';





class Catch extends Component {

  state = { fish:'1', kgs:null , wModal:false, isTimePickerVisible:false, curTime:new Date().getHours()+' : '+new Date().getMinutes(), kg:0, g:0	 }
  static navigationOptions = {
    header: null
  }

  componentWillMount(){

    this._onBackPress = this.onBackPress.bind(this) ;
    this.addListener();

    let kgs= [] ;
    let gs= [] ;

    for(var i=0; i<301 ; i++){ kgs.push(i+'') ;}
    for(var i=0; i<1000 ; i+=100){ gs.push(i+'') ;}

    this.setState({kgs:kgs, gs:gs}) ;

  }


  _showTimePicker = () => this.setState({ isTimePickerVisible: true });

  _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

  _handleDatePicked = (date) => {
    this.setState({ curTime: date.getHours()+' : '+date.getMinutes() })
    this._hideTimePicker();

};

  _updateWeight(kg , g){
    this.setState({kg:kg, g:g}) ;
  }
  openWeightModal(){
    this.setState({wModal:true}) ;
  }

  closeWeightModal(){
    this.setState({wModal:false}) ;
  }

  // on Cross or Back Press
  onBackPress(){

    // To reload hardware-back event listener on Map Activity
    this.props.navigation.state.params.refresh();
    this.props.navigation.goBack(null);
    return true ;
  }

  onAddPress(){
    // do something to record catch in redux global state
    ToastAndroid.show('Catch Added !', ToastAndroid.SHORT);
    this.props.navigation.state.params.refresh() ;
    this.props.navigation.goBack(null) ;
  }

  addListener(){
    BackHandler.addEventListener('hardwareBackPress', this._onBackPress);
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this._onBackPress);
  }



  render(){



    return (
    <View style={{ backgroundColor:'#fff', flex:1 }}>
      <StatusBar style={{ backgroundColor:'#00B89B'}}/>
      <Header
        title='ADD CATCH'
        style={{ backgroundColor:'#00B89B', alignItems:'center',height:70 ,marginBottom:0}}
        titleStyle={{ color:'#fff', fontSize:19, fontFamily:'bolder'}}
        leftButtonName='cross'
        leftButtonType='entypo'
        leftButtonColor='#fff'
        leftButtonOnPress = {this.onBackPress.bind(this)}
        rightButtonName='md-add'
        rightButtonType='ionicon'
        rightButtonColor='#fff'
        rightButtonOnPress={this.onAddPress.bind(this)}
      />
      <View style={styles.container}>

        <Card basic style={[styles.cardStyle, {height:140}]}>
          <View style={styles.iconBox}>
            <Icon  type='entypo' name='location-pin' size={35} color='#00B89B' iconStyle={{ marginBottom:10 }}/>
            <FontText style={{ color:'#8E93A8', fontSize:13, fontFamily:'bolder',alignSelf:'center'}}>LOCATION</FontText>
          </View>

          <View style={styles.infoBox1}>
            <View style={{ flex:1 }}>
                <FontText style={styles.infoHead1}>Latitude</FontText>
                <FontText style={styles.infoValue1}>27.5901{'\u00B0'}</FontText>
            </View>

            <View style={{ flex:1}}>
                <FontText style={styles.infoHead1}>Longitude</FontText>
                <FontText style={styles.infoValue1}>70.0123{'\u00B0'}</FontText>

            </View>
          </View>
        </Card>

        <Card basic style={[styles.cardStyle, {height:100}]}>

          <View style={styles.iconBox}>
            <Icon  type='material-community' name='fish' size={35} color='#00B89B' iconStyle={{  marginBottom:10 }}/>
            <FontText style={{ color:'#8E93A8', fontSize:13, fontFamily:'bolder', alignSelf:'center' }}>FISH</FontText>
          </View>
          <View style={{ flex:3 ,padding:10}}>
            <Picker
              selectedValue={this.state.fish}
              onValueChange={(itemValue, itemIndex) => this.setState({fish: itemValue})}>
              <Picker.Item label="SwordFish" value="1" />
              <Picker.Item label="Mackerel" value="2" />
              <Picker.Item label="YellowFin Tuna" value="3" />
              <Picker.Item label="BigEye Tuna" value="4" />
              <Picker.Item label="Albacore" value="5" />
              <Picker.Item label="Sardine" value="6" />
              <Picker.Item label="SwordFish" value="11" />
            </Picker>
          </View>
        </Card>

        <Card basic style={[styles.cardStyle, {height:100}]}>

          <View style={styles.iconBox}>
            <Icon  type='material-community' name='weight' size={35} color='#00B89B' iconStyle={{  marginBottom:10 }}/>
            <FontText style={{ color:'#8E93A8', fontSize:13, fontFamily:'bolder', alignSelf:'center' }}>WEIGHT</FontText>
          </View>

          <View style={{ flexDirection:'row', flex:3}}>
            <View style={styles.infoBox1}>
              <FontText style={styles.infoHead1}>Batch Weight</FontText>
              <FontText style={styles.infoValue1}>{this.state.kg} kg {this.state.g} g</FontText>
            </View>
            <View style={{ flex:-1 , alignItems:'center', justifyContent:'center', padding:10}}>
              <Icon onPress={this.openWeightModal.bind(this)} type='material-community' name='pencil' color='#929292' iconStyle={{ padding:15}}/>
            </View>
          </View>


        </Card>

        <Card basic style={[styles.cardStyle, {height:100}]}>

          <View style={styles.iconBox}>
            <Icon  type='ionicon' name='md-clock' size={35} color='#00B89B' iconStyle={{  marginBottom:10 }}/>
            <FontText style={{ color:'#8E93A8', fontSize:13, fontFamily:'bolder', alignSelf:'center' }}>TIME</FontText>
          </View>
          <View style={{ flexDirection:'row', flex:3}}>
            <View style={styles.infoBox1}>
              <FontText style={styles.infoHead1}>Catch Time</FontText>
              <FontText style={styles.infoValue1}>{this.state.curTime}</FontText>
            </View>
            <View style={{ flex:-1 , alignItems:'center', justifyContent:'center', padding:10}}>
              <Icon onPress={this._showTimePicker.bind(this)} type='material-community' name='pencil' color='#929292' iconStyle={{ padding:15}}/>
            </View>
          </View>
        </Card>

      </View>

      <Modal
        transparent={true}
        visible={this.state.wModal}
        onRequestClose={this.closeWeightModal.bind(this)}
        >
        <View style={{ flex:1 ,justifyContent:'center', alignItems:'center'}}>
          <WeightBox data={this.state.kgs} data2={this.state.gs} onPlusPress={this.closeWeightModal.bind(this)} updateWeight={this._updateWeight.bind(this)}/>
        </View>
      </Modal>
      <DateTimePicker
        isVisible={this.state.isTimePickerVisible}
        onConfirm={this._handleDatePicked}
        onCancel={this._hideTimePicker}
        mode='time'
        is24Hour={false}
      />

    </View>);
  }
}



const styles ={

  container:{ flex:1,
    backgroundColor:'#F7F8FB',
    flexDirection:'column',
    paddingTop:20,
    paddingLeft:10,
    paddingRight:10
  },

  cardStyle: {
    height:140,
    backgroundColor:'#fff',
    flexDirection:'row',
    paddingTop:15,
    paddingBottom:15
  },

  iconBox:{
    flex:1,
    borderRightWidth:0.7,
    borderColor:'#ddd',
    justifyContent:'center',
  },

  infoBox1:{
    flex:3,
    paddingLeft:20,
    flexDirection:'column',
  },

  infoHead1:{
    color:'#8E93A8',
    fontSize:14
  },

  infoValue1:{
    fontSize:17
  }


}
export default Catch ;
