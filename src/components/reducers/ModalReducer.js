import { MODAL_UPDATE } from '../actions/type' ;

const INITIAL_STATE = {
  mapExitModal: false ,
}

export default ( state = INITIAL_STATE , action) => {

  console.log(action) ;
  switch(action.type){

    case MODAL_UPDATE :
      const ns =  { ...state ,  [action.payload.key]: action.payload.status } ;
      console.log(ns) ;
      return ns ;

    default: return state ;
  }
}
