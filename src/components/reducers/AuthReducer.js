import { LOGIN_VALUE_UPDATE, LOGIN_SUCCESS, LOGIN_FAIL, AUTH_LOADING } from '../actions/type';

const INITIAL_STATE = {
  email: '',
  password: '',
  user: null,
  authloading: false,
  error: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_VALUE_UPDATE:
      return { ...state, [action.payload.type]: action.payload.value };
    case LOGIN_SUCCESS:
      return { ...state, authloading: false, error: false, user: action.payload };
    case LOGIN_FAIL:
      return { ...state, error: true, authloading: false };
    case AUTH_LOADING:
      return { ...state, error: false, authloading: true };

    default : return state;
  }
};
