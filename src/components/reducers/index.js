import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import LoadingReducer from './LoadingReducer';
import LocationReducer from './LocationReducer' ;
import ModalReducer from './ModalReducer' ;


export default combineReducers({
  auth: AuthReducer,
  loader: LoadingReducer,
  location: LocationReducer,
  modals: ModalReducer,

});
