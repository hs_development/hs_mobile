import { LOCATION_UPDATE, PATH_UPDATE } from '../actions/type' ;


const INITIAL_STATE = {
  position: {
    latitude:null ,
    longitude:null
  },
  locationFlag:false,    // initially loaded location or not
  Paths: null ,
  areas:null,

}


export default (state=INITIAL_STATE, action) => {

  switch(action.type){

    case LOCATION_UPDATE:
      return { ...state,
         position:{
           longitude: action.payload.longitude,
           latitude:  action.payload.latitude
         },
       } ;

   case PATH_UPDATE:
     return {
       ...state ,
       Paths:action.payload.Paths,
       areas: action.payload.areas,
       locationFlag: true,


     }

    default: return state ;
  }
}
