import React, { Component } from 'react';
import { View , Text,Image } from 'react-native';
import { Header, StatusBar, Card , CardSection, Chucks } from './common';
import { Icon } from 'react-native-elements';
import { StackNavigator } from 'react-navigation';




class Home extends Component {

  static navigationOptions = {
    header: null
  };

  render() {
    const degreeUni= '\u2109'
    return (

      <View style={{ backgroundColor:'#fff', flex:1 }}>

        <StatusBar />
        <Header title='HERASPACE' />
        <Card style={styles.CardStyle}>
          <CardSection style={{ justifyContent:'center'}}>
            <Icon color='#fff' size={100} name='wb-sunny' />
          </CardSection>
          <Text style={styles.tempStyles}>17{'\u2103'}</Text>
          <CardSection/>
          <View>
            <Chucks text='Sunday' font='wb-sunny' num='12'/>
            <Chucks text='Monday' font='ac-unit' num='0'/>
            <Chucks text='Tuesday' font='wb-cloudy' num='10'/>
          </View>
        </Card>
      </View>
    );
  }
}

export default Home;

const styles = {
  CardStyle: {
    backgroundColor:'#00B89B',
     borderRadius:10
  },
  tempStyles: {
    color:'#fff',
    fontSize:38,
    paddingLeft:10,
    alignSelf:'center'
  }
}
