import { FONT_LOADED } from './type';

export const fontLoaded = () => {
  return (
    {
      type: FONT_LOADED
    }
  );
};
