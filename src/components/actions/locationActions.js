import { LOCATION_UPDATE, PATH_UPDATE } from './type.js' ;
import areas from '../../data/areas.json';


// only updates location of user .
export const locationUpdate = ({ latitude , longitude })=> {

  return ({
    type : LOCATION_UPDATE ,
    payload : { latitude, longitude }
  }) ;
};

// used to update paths , fishing grounds & can be called on a  schedule .

export const pathUpdate = ({latitude , longitude }) => {

  return (dispatch) => {
    // Call API with latitude , longitude and other params for fetching Fishing Grounds 
    var response = areas ;
    var Paths = process_areas( areas.areasGreen )  ;
    dispatch({ type:PATH_UPDATE , payload:{ areas:response , Paths } }) ;
  }
};

const process_areas = (areasGreen) => {
    var Paths = []  ;
    areasGreen.map( area => {
      Paths.push(area.latlng) ;
    })

    return Paths ;
}
