import { MODAL_UPDATE } from './type'  ;

export const modalUpdate = ( { key, status }) => {
  return ({
    type: MODAL_UPDATE,
    payload: { key ,status }
  });
} ;
