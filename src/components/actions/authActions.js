import firebase from 'firebase';
import { LOGIN_VALUE_UPDATE, LOGIN_SUCCESS, LOGIN_FAIL, AUTH_LOADING } from './type';

export const loginFormUpdate = ({ type, value }) => {

  return ({
    type: LOGIN_VALUE_UPDATE,
    payload: { type, value }
  });
};

export const login = ({ email, password }, navigate) => {
    return (dispatch) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then((user) => loginSuccess(dispatch, user,navigate))
        .catch(() => dispatch({ type: LOGIN_FAIL }));
    };
};

export const authLoading = () => {
  return ({
    type: AUTH_LOADING
  });
};


const loginSuccess = (dispatch, user, navigate) => {
  dispatch({ type: LOGIN_SUCCESS, payload: user });
  navigate() ;  
};
