export const FONT_LOADED = 'font_loaded';

export const LOGIN_VALUE_UPDATE = 'login_value_changed';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_FAIL = 'login_fail';
export const AUTH_LOADING = 'auth_loading';

export const LOCATION_UPDATE = 'location_update' ;
export const PATH_UPDATE = 'path_update' ;

export const MODAL_UPDATE = 'modal_update' ; 
