import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import React, { Component } from 'react';
import { View, Image, Text, KeyboardAvoidingView } from 'react-native';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { fontLoaded, loginFormUpdate, authLoading, login } from './actions';
import { Feather, FontAwesome, EvilIcons, SimpleLineIcons } from '@expo/vector-icons' ;
import { StackNavigator } from 'react-navigation'


class LoginForm extends Component {

  static navigationOptions = {
    header: null
  };

  navigate(){
    this.props.navigation.navigate('TabBar');
  }


  onButtonPress() {
    const { email, password } = this.props;
    this.props.authLoading();
    this.props.login({ email, password} , this.navigate.bind(this));
  }

  renderButton() {
    if (this.props.authloading) return (<Spinner color='#004957' />);
    return (<Button buttonText="Login" onPress={this.onButtonPress.bind(this)} />);
  }

  renderError() {
    if (this.props.error) return (<Text style={{ color: 'red', fontSize: 18, flex: 1 }}>Authentication Failed</Text>)
  }

  render() {
    console.log(this.props)
    return (

        <View style={styles.overlay}>


            <KeyboardAvoidingView
              behavior= 'position'
              >
              <CardSection style={styles.logoStyle}>
                <Image source={require('../img/heraspace-white.png')} style={styles.logoImageStyle} />
              </CardSection>

              <CardSection>
                <Input
                placeholder="Email"
                value={this.props.email}
                onChangeText={(value) => this.props.loginFormUpdate({ type: 'email', value }) }
                >
                  <EvilIcons name="user" size={40} color="white" />
                </Input>
              </CardSection>

              <CardSection>
                <Input
                  placeholder="Password"
                  secureTextEntry
                  value={this.props.password}
                  onChangeText={(value) => this.props.loginFormUpdate({ type: 'password', value }) }
                   >
                  <SimpleLineIcons name="lock" size={25} color="white" secureTextEntry />
                </Input>
              </CardSection>

              <CardSection style={{ paddingTop: 10 }}>
                {this.renderButton()}
              </CardSection>

              <CardSection style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                {this.renderError()}
              </CardSection>

            </KeyboardAvoidingView>
        </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { email, password, authloading, error } = state.auth;
  return { email, password, authloading, error };
}

const styles = {

  container: {
    flex: 1,
  },
  background: {
    flex: 1
  },

  overlay: {
    backgroundColor: '#00B79D',
    flex:1
  },

  logoStyle: {
    justifyContent: 'center',

  },

  logoImageStyle: {
    height: 150,
    width: 150,
    paddingTop: 20,
    marginTop:100
  }
};

export default connect(mapStateToProps, { fontLoaded, loginFormUpdate, authLoading, login })(LoginForm);
