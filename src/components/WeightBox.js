import React, { Component } from 'react' ;
import { View, Picker, Dimensions  } from 'react-native' ;
import {FontText} from './common' ;
import { Icon } from 'react-native-elements';



class WeightBox extends Component {

  state = { kg:1 , g:0 } ;

  componentWillMount(){
    var {height, width} = Dimensions.get('window');
    this.width = width ;
  }

  onPickerDone() {
    this.props.updateWeight(this.state.kg , this.state.g) ;
    this.props.onPlusPress();

  }
  render(){

    return (
      <View style={{ width:this.width, justifyContent:'center', alignItems:'center', flex:1, backgroundColor:'rgba(0,0,0,0.3)'}}>
        <View style={{ width:300, justifyContent:'center', backgroundColor:'#FEFEFE'}}>

        <View style={{flexDirection:'row'}}>
          <View style={{flex:1, backgroundColor:'#00C6AF'}}>
            <Picker
              style={{ width:50, alignSelf:'center',backgroundColor:'transparent', color:'#fff'}}
              mode='dialog'
              selectedValue={this.state.kg}
              onValueChange={(itemValue, itemIndex) => this.setState({kg: itemValue})}>
              {
                this.props.data.map((item,index)=> {
                  return (<Picker.Item label={item}  value={index} key={index} />) ;
                })

              }
            </Picker>

          </View>

          <View style={{flex:1, backgroundColor:'#D5D1DA', alignItems:'center', justifyContent:'center'}}>
            <FontText style={{color:'#fff', fontFamily:'bolder',}}>KILOGRAM</FontText>
          </View>
        </View>

        <View style={{ flexDirection:'row' , padding:15 , alignItems:'center'}}>

          <FontText style={{ color:'#00B89B', fontSize:13, fontFamily:'bolder', flex:1 , }}>INPUT BATCH WEIGHT</FontText>
          <Icon onPress={this.onPickerDone.bind(this)} name='check' size={36} type='evilicon' color='#00B89B' />

        </View>


        <View style={{flexDirection:'row'}}>



          <View style={{flex:1, backgroundColor:'#00C6AF'}}>
            <Picker
              style={{ width:50, alignSelf:'center',backgroundColor:'transparent', color:'#fff'}}
              mode='dialog'
              selectedValue={this.state.g}
              onValueChange={(itemValue, itemIndex) => {this.setState({g: itemValue})

            }}>
              {
                this.props.data2.map((item,index)=> {
                  return (<Picker.Item label={item}  value={item} key={index} />) ;
                })

              }
            </Picker>

          </View>

          <View style={{flex:1, backgroundColor:'#D5D1DA', alignItems:'center', justifyContent:'center'}}>
            <FontText style={{color:'#fff', fontFamily:'bolder',}}>GRAMS</FontText>
          </View>



        </View>


      </View>
    </View>

    );
  }
}

const styles = {

}

export default WeightBox ;
