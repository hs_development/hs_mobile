import React, { Component } from 'react';
import { View , Text } from 'react-native';
import {StatusBar, Header, Card , CardSection} from './common';
import fish from '../data/fish.json' ;
import CustomMultiPicker from "react-native-multiple-select-list";
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Button } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import { Platform, BackHandler } from 'react-native'


class Trips extends Component {

  static navigationOptions = {
    header: null
  };



  onCrossPress(){

    this.props.navigation.goBack(null);

  }

  onNextPress(){
    this.props.navigation.navigate('Time');
  }

  componentWillUnmount(){
  }

  render() {
    return (
      <View style={{ backgroundColor:'#00B89B', flex:1 }}>

        <StatusBar style={{ backgroundColor:'#00B89B'}}/>


        <Header
          title='SELECT FISH SPECIES'
          titleStyle={{color:'#fff',
                      fontSize:19
                    }}
          leftButtonName='cross'
          leftButtonType='entypo'
          leftButtonColor='#fff'
          leftButtonOnPress = {this.onCrossPress.bind(this)}
         />


        <View style={{ padding:0 , marginBottom:20 , flex:8 }}>
          <View style={{ flex:1 , flexDirection:'column' , alignItems:'center'}}>
            <MaterialCommunityIcons name='fish' color='#fff' size={70} />
          </View>
          <View style={{flex:5 ,padding:6}}>
            <CustomMultiPicker
              options={fish}
              multiple={true} //
              placeholder={"Search"}
              placeholderTextColor={'#fff'}
              returnValue={"label"} // label or value
              callback={(res)=>{ console.log(res) }} // callback, array of selected items
              rowBackgroundColor={"#fff"}
              rowHeight={40}
              rowRadius={0}
              iconColor={"#00B89B"}
              iconSize={30}
              selectedIconName={"ios-checkmark-circle"}
              unselectedIconName={"ios-radio-button-off-outline"}
              selected={['SwordFish', 'Mackerel']}
            />
          </View>
        </View>



        <View style={{ flex:2 }}>
          <Button
            raised
            iconRight={{name: 'navigate-next'}}
            title='Next'
            buttonStyle={{backgroundColor:'#004957' }}
            onPress={this.onNextPress.bind(this)}
           />

        </View>


      </View>
    );
  }
}

export default Trips;
