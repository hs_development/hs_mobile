import React, { Component } from 'react';
import { View , Text } from 'react-native';
import {StatusBar, Header, Card , CardSection} from './common';

class Profile extends Component {

  static navigationOptions = {
    header: null
  };
  
  render() {
    return (
      <View style={{ backgroundColor:'#fff', flex:1 }}>
        <StatusBar />
        <Header title='HERASPACE' />
      </View>
    );
  }
}

export default Profile;
