import React, { Component } from 'react';
import { View , Text , Image, BackHandler, Modal } from 'react-native';
import {MapView}  from 'expo';
import ActionButton from 'react-native-action-button';
import MapStyle from '../data/MapStyle.json';
import { ActionModal } from './common' ;
import { connect } from 'react-redux';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { locationUpdate, pathUpdate, modalUpdate } from './actions'




class Map extends Component {


	static navigationOptions = {
		header: null
	};

	onRefresh(){
		BackHandler.addEventListener('hardwareBackPress', this._onBackPress) ;
	}

	onRequestClose(){
		this.props.modalUpdate({ key:'mapExitModal',status:false}) ;
	}

	onBackPress() {


		if(this.props.mapExitModal) {
			this.props.modalUpdate({ key:'mapExitModal',status:false}) ;
			return false;
		}

		this.props.modalUpdate({ key:'mapExitModal',status:true}) ;
		return true ;
	}

	onCloseMap(){
		this.props.modalUpdate({ key:'mapExitModal',status:false}) ;
		this.props.navigation.goBack(null);
	}

	update_location(){

		this.watchId = navigator.geolocation.watchPosition(
			(position) => {

				this.props.locationUpdate({ latitude : position.coords.latitude , longitude: position.coords.longitude }) ;
				this.props.pathUpdate({ latitude : position.coords.latitude , longitude: position.coords.longitude }) ;
			},
			(error) => console.log('problem'),
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 0, distanceFilter: 1},

			);
	}

  componentWillMount(){
		this._onBackPress = this.onBackPress.bind(this) ;
		this._onRefresh = this.onRefresh.bind(this) ;
		this.update_location();


		this._onRefresh(); // adding onBack Listener when return from Catch Activity

  }


	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress', this._onBackPress) ;
	}



  render(){

				if(this.props.locationFlag) {
					console.log(this.props) ;

          return (
            <View style={Styles.container}>
              <MapView
                customMapStyle={MapStyle}
                style={Styles.map}
                initialRegion={{
                  latitude: this.props.position.latitude,
                  longitude: this.props.position.longitude,
                  latitudeDelta: 3.0922,
                  longitudeDelta: 3.0421,
                }}>

                <MapView.Marker
                  coordinate= {{latitude: this.props.position.latitude , longitude: this.props.position.longitude }}
                  key={"MainMarker"}>


                      <View
                        source={require('../img/boat.png')}
                        style={Styles.boatStyle}
                      />


                </MapView.Marker>



                {this.props.areas.areasGreen.map((marker,i) => (
                   <MapView.Marker
                     coordinate={marker.latlng}
                     key={i}
                   >


                     <View style={Object.assign(Styles.circle)}>
                        <Text style={Styles.fishType}>{marker.Type}</Text>
                        <Text style={Styles.fishQuantity}>{marker.kilos} Kg</Text>
                      </View>

                      <MapView.Callout style={Styles.calloutStyle} tooltip={true}>
                        <View style={Styles.containerStyle}>
                          <Text style={Styles.text1}>Species</Text>
                          <Text style={Styles.text2}>{marker.Type}</Text>
                        </View>

                        <View style={Styles.containerStyle}>
                          <Text style={Styles.text1}>Quantity</Text>
                          <Text style={Styles.text2}>{marker.kilos}</Text>
                        </View>

                        <View style={Styles.containerStyle}>
                          <Text style={Styles.text1}>Distance</Text>
                          <Text style={Styles.text2}>170 km</Text>
                        </View>

                      </MapView.Callout>
                   </MapView.Marker>
                  ))

                }


                {this.props.areas.areasRed.map((marker,j) => (
                   <MapView.Marker
                     coordinate={marker.latlng}
                     key={1000+j}
                   >
                     <View style={Object.assign(Styles.circle2)}>
                        <Text style={Styles.fishType}>{marker.Type}</Text>
                        <Text style={Styles.fishQuantity}>{marker.kilos} Kgs</Text>
                      </View>
                   </MapView.Marker>
                  ))

                }

                    <MapView.Polyline
                    coordinates={[this.props.position].concat(this.props.Paths)}
                    strokeColor='#fff'
                    strokeWidth={0.2}
                  />


              </MapView>

							<ActionButton
								buttonColor='#00B89B'
								fixNativeFeedbackRadius
								onPress={()=>{console.log('hii')}}>

								<ActionButton.Item
									buttonColor='#00B89B'
									title="Catch"
									onPress={() =>{
										this.props.navigation.navigate('Catch', {
											refresh:this._onRefresh ,
										}) ;
										BackHandler.removeEventListener('hardwareBackPress', this._onBackPress) ;

									}}
									useNativeFeedback={false}
								>
									<MaterialCommunityIcons name='fish' color='#fff' size={30} />
								</ActionButton.Item>

							</ActionButton>

							<Modal
								transparent={true}
								visible={this.props.mapExitModal}
								onRequestClose={this.onCloseMap.bind(this)}
								>
								<View style={{ flex:1 ,justifyContent:'center', alignItems:'center'}}>
									<ActionModal onPress1={this.onRequestClose.bind(this)} onPress2={this.onCloseMap.bind(this)} />
								</View>
							</Modal>
            </View>);
					}

			else return(
				<View style={{ backgroundColor:'#fff', justifyContent:'center', alignItems:'center'}}>
						<Text style={{ color:'#2C2E3B' }}> Finding Closest Fishing Grounds</Text>
				</View>
			);

  }

}

const mapStateToProps = (state) => {
		const { position, locationFlag , Paths , areas } = state.location ;
		const { mapExitModal } = state.modals ;

		return { position , locationFlag , Paths , areas, mapExitModal } ;
}


const Styles= {


  spinnerContainerStyle:{
    alignSelf:'center'
  },

  mainContiner: {
    flex:1,
    flexDirection:'column',
    justifyContent:'flex-start'
  },

  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  areaTextStyle:{
    color: "#fff",
    fontSize: 18
  },

  circle: {
      width: 60,
      height: 60,
      borderRadius: 40 ,
      borderWidth: 2,
      flexDirection:'column',
      justifyContent:'space-around',
      paddingTop:5,
      borderColor:'green'

  },

  circle2: {
      width: 60,
      height: 60,
      borderRadius: 40 ,
      borderWidth: 2,
      flexDirection:'column',
      justifyContent:'space-around',
      paddingTop:5,
      borderColor: 'red'

  },


  fishType: {
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
      fontSize: 10,
      marginBottom: 10,

  },

  fishQuantity: {
    color: 'white',
    textAlign: 'center',
    fontSize: 8,
    fontWeight: 'bold',
    marginBottom: 10
  },

  calloutStyle: {
    height: 150,
    width: 150,
    paddingBottom :5
  },

  containerStyle:{
    flex:1,
    flexDirection:'row',
    height:40,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    backgroundColor: '#1565C0',
    borderRadius:5,
    justifyContent: 'center',
    marginBottom:5,
    marginTop:5,

    padding:7,
    paddingTop:12

  },

  text1:{
    color:"#000",
    fontWeight:'bold',
    flex:1 ,
    fontSize:12

  },

  text2:{
    color:"#fff",
    fontWeight:'bold',
    flex:1 ,
    fontSize:11
  },

  boatStyle:{
  	width: 15,
    height: 15,
    borderRadius: 30/2,
    backgroundColor: '#00B89B'
  }

}
export default connect( mapStateToProps, { locationUpdate, pathUpdate, modalUpdate })(Map) ;
