import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { connect } from 'react-redux';


const Input = (props) => {


  return (
    <View style={styles.containerStyle}>
      <View style={styles.iconStyle}>
        {props.children}
      </View>
      {loadInput(props)}
    </View>
  );
};

const loadInput = (props) => {
  const { label, value, onChangeText, placeholder, secureTextEntry, onFocus } = props ;

  if (props.fontLoaded) {
    return (
      <TextInput
        secureTextEntry={secureTextEntry}
        style={styles.inputStyle}
        value={value}
        onChangeText={onChangeText}
        autoCorrect={false}
        placeholder={placeholder}
        underlineColorAndroid='rgba(0,0,0,0)'
        placeholderTextColor='#fff'
        onFocus={onFocus}
      />
    );
  }
};

const mapStateToProps = (state) => {
  const { fontLoaded } = state.loader;
  return { fontLoaded };
};

export default connect(mapStateToProps, {})(Input);

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    height:60,
    backgroundColor: 'rgba(255,255,255,0.4)',
    borderRadius:10

  },

  iconStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },



  inputStyle: {
    flex: 5,
    lineHeight: 23,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize:15,
    color: '#00333F',
    fontFamily: 'jumpy'
  }

};
