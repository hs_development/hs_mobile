import React from 'react' ;
import { View } from 'react-native' ;



const basic = (flag) => {
  if(flag) return ({
    padding:5,
    borderBottomWidth: 1,
    borderColor:'#ddd',
    backgroundColor: '#fff',
  })
}
const CardSection = (props) => {
  return (
    <View style={[styles.containerStyle, basic(props.basic) ,props.style]}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle:{
    padding: 15 ,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position:'relative',
 }




};

export { CardSection }
