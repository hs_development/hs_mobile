import React from 'react';
import { View } from 'react-native';



const basic = (flag)=>{

  const basic = {
      elevation:4,
      borderWidth:1,
      borderColor:'#ddd',
      borderBottomWidth: 0,
      shadowColor: '#000',
      shadowOffset: {width:0 , height: 4},
      shadowOpacity:1 ,
    }


  if(flag) return basic ;
}


const radius = (flag) => {

  const radius = {
    borderRadius:2,
    shadowRadius: 2
  }

  if(flag) return radius ;
}

const Card = (props) => {
  return (
    <View style={[styles.container, basic(props.basic) , radius(props.radius), props.style]}>
      {props.children}
    </View>
  );
};

const styles = {
  container: {
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,

  }

};





export { Card };
