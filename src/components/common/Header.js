import { View, Text } from 'react-native';
import React from 'react';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';


const Header = (props) => {
  return(
    <View style={[styles.containerStyle , props.style]} >
      <View style={styles.leftButtonStyle}>{loadleftButton(props)}</View>
      {loadFont(props)}
      <View />
      <View style={styles.rightButtonStyle}>{loadRightButton(props)}</View>

    </View>
  );
}


const loadleftButton = (props) => {
  if(props.leftButtonName !== undefined) return (
    <Icon
      name={props.leftButtonName}
      type={props.leftButtonType}
      color={props.leftButtonColor}
      onPress={props.leftButtonOnPress}
      underlayColor='#00C6AF' //  to prevent white Highlight onPress
    />
  )

  return <View />
};



const loadRightButton = (props) => {
  if(props.rightButtonName !== undefined ) return(
    <Icon
      name={props.rightButtonName}
      type={props.rightButtonType}
      color={props.rightButtonColor}
      onPress={props.rightButtonOnPress}
      underlayColor='#00C6AF'
      iconStyle={{ padding:10}}
 />
  );

  return <View />
}

const loadFont = (props) => {
  if (props.fontLoaded) {
    return <View style={{flex:1, flexDirection:'row' ,justifyContent:'center', paddingRight:20}}><Text style={[styles.TextStyle , props.titleStyle]} >{props.title}</Text></View>
  }
};

const mapStateToProps = (state) => {
  const { fontLoaded } = state.loader;
  return { fontLoaded };
};


export default connect(mapStateToProps, {})(Header);

const styles = {
  containerStyle: {
    flexDirection: 'row',
    height: 50,
    backgroundColor: 'transparent',
    marginBottom: 10,
    paddingLeft:5,
    alignItems:'flex-end',
  },

  TextStyle: {
    fontFamily:'bolder',
    fontSize: 24 ,
    color: '#00B89B',
  },

  leftButtonStyle:{
    paddingLeft:10,
    flex:-1,
  },

  rightButtonStyle:{
    paddingRight: 10,
    flex:-1,
  },

};
