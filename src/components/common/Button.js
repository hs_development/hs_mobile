import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

const Button = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} style={[styles.buttonStyle, props.buttonStyle]}>
      {loadText(props)}
    </TouchableOpacity>

  );
};


const loadText = (props) => {
  const { buttonText } = props;

  if (props.fontLoaded) {
    return (
      <Text style={[styles.textStyle, props.textStyle]}>{buttonText}</Text>
    );
  }
};

const mapStateToProps = (state) => {
  const { fontLoaded } = state.loader;
  return { fontLoaded };
};


const styles = {

  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    fontFamily: 'bolder'
  },

  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#004957',
    borderRadius: 5,
    marginLeft: 5,
    marginRight: 5
  }

};

export default connect(mapStateToProps, {})(Button);
