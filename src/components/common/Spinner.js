import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = (props) => {
  return (
    <View style={Styles.SpinnerStyle}>
      <ActivityIndicator size={props.size || 'large'} color={props.color} />
    </View>
  );
};


const Styles = {
    SpinnerStyle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }


};

export { Spinner };
