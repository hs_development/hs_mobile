import { Text , View } from 'react-native';
import { Icon } from 'react-native-elements';
import React from 'react';

const Chucks = (props)=> {
  return(
    <View style={{ paddingLeft:10 , paddingRight:10 , marginBottom:10}}>
      <View style={styles.container}>
        <Text style={{ fontSize:16 , color:'white' , fontWeight:'bold' , flex:1}}>{props.text}</Text>
        <View style={{flexDirection:'row' , flex:1 , justifyContent:'flex-end'}}>
          <Text style={{ fontSize:16 , color:'white',marginRight:10,fontWeight:'bold'}}>{props.num+'\u2103'}</Text>
          <Icon name={props.font} size={20} color='#fff' />
        </View>

      </View>
    </View>
  );

}

export default Chucks ;

const styles = {
  container: {
    alignSelf: 'stretch',
    padding:20,
    flexDirection:'row',
    justifyContent:'space-between',
    backgroundColor:'rgba(0, 198, 175, 0.6)',
    borderRadius:6
  }
}
