import { Feather, EvilIcons} from '@expo/vector-icons';
import { Text, View } from 'react-native';
import React, { Component } from 'react';

class TabIcon extends Component {
  render() {
     var color = this.props.selected ? '#00f240' : '#301c2a';
    return (
      <View style={styles.containerStyle}>
        <Feather name={this.props.iconName} color={color} size={32} />
        <Text style={{ color, fontSize: 12 }} >{this.props.title}</Text>
      </View>
    );
  }
}

export default TabIcon;

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
};
