import React, { Component } from 'react' ;
import { View  } from 'react-native';
import { connect } from 'react-redux';
import Button from './Button' ;
import FontText from './FontText' ;

class ActionModal extends Component {


  render(){
    return (
      <View style={styles.containerStyle}>
         <View style={styles.textContainerStyle}>
           <FontText style={styles.textStyle}>Do You Want To Exit The Trip ? </FontText>
         </View>

         <View style={styles.buttonGroupStyle}>
           <Button buttonStyle={styles.buttonStyle1} buttonText="No" onPress={this.props.onPress1} textStyle={styles.buttonTextStyle} />
           <Button buttonStyle={styles.buttonStyle2} buttonText="Yes" onPress= {this.props.onPress2} textStyle={styles.buttonTextStyle}/>
         </View>
      </View>
    );
  }
};



const styles = {
  containerStyle : {
    width: 300,
    backgroundColor: '#fff',
    flexDirection: 'column'
  },

  textContainerStyle:{
      justifyContent:'center',
      alignItems:'center',
      paddingLeft:20,
      paddingRight:20,
      height:160
  },

  buttonStyle1:{
    marginLeft:0 ,
    marginRight:0,
    backgroundColor:'#00B89B',
    borderRadius:0 ,
    padding:0,
    height:50
  },

  buttonStyle2:{
    marginLeft:0 ,
    marginRight:0,
    backgroundColor:'#CCCCCC',
    borderRadius:0 ,
    padding:0,
    height:50
  },

  buttonGroupStyle:{
    flexDirection: 'row',

  },

  buttonTextStyle:{
    fontFamily:'bolder',
    fontSize:14,
    color:'#fff'
  },

  textStyle:{
    color:'#929292',
    fontFamily:'bolder',
    fontSize: 14 ,
  }


}


export default ActionModal ;
