import React, {Component} from 'react';
import {View, Text, StyleSheet, Platform} from 'react-native';

class StatusBar extends Component{
  render(){
    return(
      <View style={[styles.statusBarBackground, this.props.style || {}]} />
    );
  }
}

const styles = {
  statusBarBackground: {
    height: (Platform.OS === 'ios') ? 40 : 24,
    backgroundColor:'#fff',

  }
}

export {StatusBar} ;
