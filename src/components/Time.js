import React, { Component } from 'react';
import { View , Text } from 'react-native';
import {StatusBar, Header, Card , CardSection, CircularSlider } from './common';
import { Slider } from 'react-native-elements';
import { Button } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';



class Time extends Component {



  static navigationOptions = {
    header: null
  };


  onBackPress(){
    this.props.navigation.goBack(null);
  }
  onNextPress(){
    this.props.navigation.dispatch({
      type:'replaceCurrentScreen',
      routeName:'Map',
      key:'Map'
    });
  }

  constructor(props){
    super(props)
    this.state = {
      slider1: 100,
    }
  }

  render() {

    var time =   ((this.state.slider1/10)+6)
    return (
      <View style={{ backgroundColor:'#00B89B', flex:1 }}>
        <StatusBar style={{ backgroundColor:'#00B89B'}}/>
        <Header
          title='TRIP TIME'
          titleStyle={{color:'#fff',
                      fontSize:19
                    }}
          leftButtonName='chevron-thin-left'
          leftButtonType='entypo'
          leftButtonColor='#fff'
          leftButtonOnPress = {this.onBackPress.bind(this)}
         />
        <View style={styles.root}>
          <Text style={{ alignSelf:'center' , fontSize:40 , color:'#fff' , fontWeight:'bold'}}>{time}HR</Text>
        <View style={styles.container}>
          <CircularSlider width={200} height={200} meterColor='#fff' textColor='#fff'
                value={this.state.slider1} onValueChange={(value)=>this.setState({slider1:value})}/>
          <View style={styles.slider1}>
          </View>
        </View>
      </View>


        <View style={{ flex:2 }}>
          <Button
            raised
            iconRight={{name: 'navigate-next'}}
            title='Next'
            buttonStyle={{backgroundColor:'#004957' }}
            onPress={this.onNextPress.bind(this)}
           />
        </View>
      </View>
    );
  }
}


const styles = {

  root: {
    flex:5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    position: 'relative',
    width: 200,
    height: 200
  },

  slider1: {
    position: 'absolute',
    top: 0,
    left: 0
  },
  slider2: {
    position: 'absolute',
    top: 25,
    left: 25
  }
};


export default Time;
