import React , { Component } from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import LoginForm from './components/LoginForm';
import Home from './components/Home';
import History from './components/History';
import Profile from './components/Profile';
import Trip from './components/Trips' ;
import Time from './components/Time' ;
import Map from './components/Map' ;
import Catch from './components/Catch' ;
import  stats from './components/stats' ;
import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import { Font } from 'expo';
import { fontLoaded } from './components/actions';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';

import  Blank from './components/Blank' ;


class Router extends Component{

  async componentDidMount() {
    await Font.loadAsync({
      'jumpy': require('../assets/Quicksand-Regular.ttf'),
      'bolder': require('../assets/sans.ttf'),
    });

    this.props.fontLoaded();
  }


  render(){
    return (
      <RootStack />
    )
  }

}

const TripStack = StackNavigator(
  {
    Fish:{
      screen:Trip
    },

    Time:{
      screen:Time
    }

  },
  {
    initialRouteName:'Fish'
  }

);


const TabView = TabNavigator(
  {
    HOME:{
      screen:Home,
      navigationOptions:{
        tabBarLabel:'HOME',
        tabBarIcon: ({ tintColor }) => <Icon color={tintColor} name={"home"} size={23}/>
      }
     },
    RECORDS: {
       screen:History,
       navigationOptions:{
         tabBarLabel:'RECORDS',
         tabBarIcon: ({ tintColor }) => <Icon color={tintColor} name={"directions-boat"} size={23}/>
       }
     },


    ADD: {
       screen:Blank ,
       navigationOptions:{
         tabBarIcon: ({ tintColor }) =>{ return <Icon color={'#00B89B'} name={"add-circle"} size={26}/>},
       }
     },
    STATS: {
       screen:stats,
       navigationOptions:{
         tabBarLabel:'STATS',
         tabBarIcon: ({ tintColor }) => <Icon color={tintColor} name={"pie-chart"} size={23}/>
       }
     },
    PROFILE: {
      screen:Profile,
      navigationOptions:{
        tabBarLabel:'PROFILE',
        tabBarIcon: ({ tintColor }) => <Icon color={tintColor} name={"person"} size={23}/>
      }
    }
  },
  {
    tabBarComponent: ({jumpToIndex, ...props, navigation})=>(
      <TabBarBottom
        {...props}
        jumpToIndex= {(index)=>{
          if(index==2) navigation.navigate('TripStack') ;
          else jumpToIndex(index)
        }}
        style={ { height:59, backgroundColor: '#fff',borderTopWidth: Platform.OS === 'ios' ? 0 : StyleSheet.hairlineWidth,
        borderTopColor: 'rgba(0, 0, 0, .1)',
        paddingBottom:7
      }}
        labelStyle= {{ fontSize:10 }}
      />
    ),
    swipeEnabled:false,
    animationEnabled:false,
    tabBarPosition:'bottom',
    tabBarOptions: {
      upperCaseLabel:true,
      indicatorStyle:{
        backgroundColor: '#fff'
      },
      showIcon: true,
      activeTintColor: '#00B89B',
      inactiveTintColor:'#B3B3B3',

    }

  }
)

const RootStack = StackNavigator(
  {
    Login:{
      screen:LoginForm
    },

    TabBar:{
      screen:TabView
    },

    TripStack:{
      screen:TripStack
    },
    Map:{
      screen:Map
    },
    Catch:{
      screen:Catch
    }

  },
  {
    initialRouteName:'TabBar'
  }

);

const getStateForAction = RootStack.router.getStateForAction;

RootStack.router.getStateForAction = (action,state) => {
  if(action.type==='replaceCurrentScreen' && state){
    const routes = state.routes.slice(0,state.routes.length-1);
    routes.push(action) ;
    return {
      ...state,
      routes,
      index:routes.length-1,
    }
  }
  return getStateForAction(action,state);
};


export default connect(null , {fontLoaded}) (Router) ;
